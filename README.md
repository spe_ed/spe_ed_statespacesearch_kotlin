<!--
*** Build using the Best-README-Template.
-->

<!-- PROJECT LOGO -->
<br />
<p align="center">
![SPE_ED](spe_ed_logo_side.png "SPE_ED Automat")
  <h3 align="center">SPE_ED State Space Search Agent in Kotlin</h3>

  <p align="center">
    Implementation of an Agent for the game spe_ed using State Space Search.<br />
    This is a project developed for the 2021 InformatiCup
    <br />
    <a href="https://spe_ed.pages.gwdg.de/html/-state-space-search_-kotlin/index.html"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.gwdg.de/spe_ed/spe_ed_statespacesearch_kotlin/issues">Report Bug</a>
    ·
    <a href="https://gitlab.gwdg.de/spe_ed/spe_ed_statespacesearch_kotlin/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#built-with">Built With</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation-docker">Installation</a></li>
        <li><a href="#installation-without-docker">Installation without Docker</a></li>
      </ul>
    </li>
    <li>
      <a href="#usage">Usage</a>
    </li>
    <li>
      <a href="#extending-functionality">Extending Functionality</a>
    </li>
  </ol>
</details>

### Built With

<div style="display: -ms-flexbox;     display: -webkit-flex;     display: flex;     -webkit-flex-direction: row;     -ms-flex-direction: row;     flex-direction: row;     -webkit-flex-wrap: wrap;     -ms-flex-wrap: wrap;     flex-wrap: wrap;     -webkit-justify-content: space-around;     -ms-flex-pack: distribute;     justify-content: space-around;     -webkit-align-content: stretch;     -ms-flex-line-pack: stretch;     align-content: stretch;     -webkit-align-items: flex-start;     -ms-flex-align: start;     align-items: flex-start;">
<a href="https://kotlinlang.org/"><img src="logo.svg" alt="Kotlin" width="64" height="64" title="Kotlin"></a>
<a href="https://gradle.org/"><img src="https://gradle.org/images/gradle-knowledge-graph-logo.png" alt="Gradle" width="64" height="64" title="Gradle"></a>
<a href="https://www.docker.com/"><img src="https://www.docker.com/sites/default/files/d8/2019-07/vertical-logo-monochromatic.png" alt="Docker" width="64" height="64" title="Docker"></a>
</div>


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites
* [Docker](https://docs.docker.com/get-docker/)

### Installation Docker
Running this project in Docker won't produce a visual output, DISPLAY_GAME has to be false

1. Clone the repo
  ```sh
  git clone https://gitlab.gwdg.de/spe_ed/spe_ed_statespacesearch_kotlin.git
  ```
2. Use Docker to build the image
  ```sh
  docker build -t spe_ed_agent .
  ```
3. Use Docker to run the image
  ```sh
  docker run -e URL="<URL>" -e KEY="<API key>" -e TIME_URL="<TIME URL>" spe_ed_agent
  ```

  ### Installation without Docker
To use the DISPLAY_GAME option, the agent has to be run outside Docker to create a JFrame.
To be able to view the game board even after the game has ended, the JFrame will remain open. 
Therefore only the docker installation should be used in the competition.
1. Clone the repo
  ```sh
  git clone https://gitlab.gwdg.de/spe_ed/spe_ed_statespacesearch_kotlin.git
  ```
2. Set the environment variables
  ```sh
  URL=<URL>
  KEY=<API KEY>
  TIME_URL=<TIME URL>
  DISPLAY_GAME=<true / false> # Default is false
  ```
3. Use Gradle wrapper to run the programm
  ```sh
  ./gradlew run
  ```

<!-- USAGE EXAMPLES -->
## Usage

Use the ENV in the Dockerfile to set a custom game url, if the offical servers aren't available and
 ```sh
docker-compose up --build
```
to build and start a new container with the env values overwritten

To start multiple players use
```sh
docker-compose up --scale client=<player number> --build
```

_For more info, please refer to the [Documentation](https://spe_ed.pages.gwdg.de/html/-state-space-search_-kotlin/index.html)_

<!-- Extension EXAMPLES -->
## Extending Functionality

To realize your own ideas with state space search, use the provided abstract `Agent` class and implement your own client.

To change the scoring of states just overwrite the `staticEval` function

You can find examples of this in the codebase, as all 3 provided agents use the `Agent` class

To use our linting rules on commit run 
```sh
gradle addKtlintCheckGitPreCommitHook
```

_For more info, please refer to the [Documentation](https://spe_ed.pages.gwdg.de/html/-state-space-search_-kotlin/index.html)_
