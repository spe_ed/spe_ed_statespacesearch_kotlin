package agent

import TestDataProvider
import game.Cell
import game.Direction
import game.Move
import game.Player
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.powermock.reflect.Whitebox
import java.time.Instant
import kotlin.math.pow
import kotlin.test.assertNotEquals
import kotlin.test.assertNotNull

internal class AgentTest {

    private var testAgent: Agent? = null

    @BeforeEach
    fun setUp() {
        // Reset TestAgent
        testAgent = object : Agent(TestDataProvider().getTestGameState()) {
            override fun makeMove(roundStartTime: Instant): String {
                return Move.change_nothing.toAction()
            }

            override fun staticEval(player: Player, blockedCells: List<Cell>, stepsInTheFuture: Int): Double {
                return getFractionOfAvailableCellsInKernel(player, blockedCells)
            }
        }
    }

    @Test
    fun evalState() {
        val score = Whitebox.invokeMethod<Double>(
            testAgent,
            "evalState",
            listOf(Move.change_nothing, Move.turn_right, Move.change_nothing),
        )

        val score2 = Whitebox.invokeMethod<Double>(
            testAgent,
            "evalState",
            listOf(Move.slow_down),
        )

        assertNotNull(score, "Valid Moves should result in score")

        assert(score in 0.0..1.0) { "Score should be between 0 and 1" }

        assertNull(score2, "Invalid Moves should not result in score")
    }

    @Test
    fun getGameStateFromMoves() {
        val player = testAgent?.rootGameState?.players?.get("1")!!

        val state = Whitebox.invokeMethod<Pair<List<Cell>, Player>?>(
            testAgent,
            "getGameStateFromMoves",
            listOf(Move.change_nothing, Move.turn_right, Move.change_nothing),
        )

        val state2 = Whitebox.invokeMethod<Pair<List<Cell>, Player>?>(
            testAgent,
            "getGameStateFromMoves",
            listOf(Move.slow_down)
        )

        assertNotNull(state, "Valid move should work")

        assertNull(state2, "Invalid move should not work")

        val (_, newPlayer) = state

        assertNotEquals(player.x, newPlayer.x, "Move should change x Position of player")
        assertNotEquals(player.y, newPlayer.y, "Move should change y Position of player")
    }

    @Test
    fun getGameStateFromMove() {
        val player = testAgent?.rootGameState?.players?.get("1")!!
        val newState = Whitebox.invokeMethod<Pair<List<Cell>, Player>?>(
            testAgent,
            "getGameStateFromMove",
            Move.change_nothing,
            listOf<Cell>(),
            player,
            1
        )

        assertNotNull(newState, "Valid move should work")

        val (_, newPlayer) = newState

        assertNotEquals(player.y, newPlayer.y, "Move should change y Position of player")
    }

    @Test
    fun movePlayerSpeed1() {
        val player = testAgent?.rootGameState?.players?.get("1")!!
        val x = player.x
        val y = player.y
        val newState = Whitebox.invokeMethod<Pair<List<Cell>, Player>?>(
            testAgent,
            "movePlayer",
            testAgent?.rootGameState?.players?.get("1"),
            listOf<Cell>(),
            1
        )

        assertNotNull(newState, "Valid move should work")

        val (cells, _) = newState

        assert(player.x != x || player.y != y) { "Expect function to change player coordinates" }

        assertEquals(player.speed, cells.size, "Expect function to block cells equal to the speed of the player")
    }

    @Test
    fun movePlayerSpeed5() {
        val player = testAgent?.rootGameState?.players?.get("1")!!
        player.speed = 5
        val x = player.x
        val y = player.y
        val newState = Whitebox.invokeMethod<Pair<List<Cell>, Player>?>(
            testAgent,
            "movePlayer",
            testAgent?.rootGameState?.players?.get("1"),
            listOf<Cell>(),
            1
        )

        assertNotNull(newState)

        val (cells, _) = newState

        assert(player.x != x || player.y != y) { "Expect function to change player coordinates" }

        assertEquals(player.speed, cells.size, "Expect function to block cells equal to the speed of the player")
    }

    @Test
    fun movePlayerJump() {
        val player = testAgent?.rootGameState?.players?.get("1")!!
        player.speed = 9
        val x = player.x
        val y = player.y
        val newState = Whitebox.invokeMethod<Pair<List<Cell>, Player>?>(
            testAgent,
            "movePlayer",
            testAgent?.rootGameState?.players?.get("1"),
            listOf<Cell>(),
            6
        )

        assertNotNull(newState)

        val (cells, _) = newState

        assert(player.x != x || player.y != y) { "Expect function to change player coordinates" }

        assertEquals(2, cells.size, "Expect function to block less cells because jump")
    }

    @Test
    fun movePlayerIntoWall() {
        val player = testAgent?.rootGameState?.players?.get("1")!!
        player.speed = 9
        player.direction = Direction.up
        val newState = Whitebox.invokeMethod<Pair<List<Cell>, Player>?>(
            testAgent,
            "movePlayer",
            testAgent?.rootGameState?.players?.get("1"),
            listOf<Cell>(),
            1
        )

        assertNull(newState, "Expect move to fail if player moves into wall")
    }

    @Test
    fun getAllCellsInKernel() {
        val player = testAgent?.rootGameState?.players?.get("1")!!
        val size = 5
        val cells = Whitebox.invokeMethod<List<List<Cell?>>>(
            testAgent,
            "getAllCellsInKernel",
            player,
            size
        )
        assertEquals((size * 2 + 1).toDouble().pow(2).toInt(), cells.flatten().size, "Expect function to get all cells")
    }

    @Test
    fun getFractionOfEnemiesInKernel() {
        val score1 = Whitebox.invokeMethod<Double>(
            testAgent,
            "getFractionOfEnemiesInKernel",
            testAgent?.rootGameState?.players?.get("1"),
            10
        )

        val score2 = Whitebox.invokeMethod<Double>(
            testAgent,
            "getFractionOfEnemiesInKernel",
            testAgent?.rootGameState?.players?.get("1"),
            10
        )

        testAgent?.rootGameState?.players?.get("2")?.x = testAgent?.rootGameState?.players?.get("1")?.x?.plus(1)!!
        testAgent?.rootGameState?.players?.get("2")?.y = testAgent?.rootGameState?.players?.get("1")?.y!!

        val score1WithNewEnemy = Whitebox.invokeMethod<Double>(
            testAgent,
            "getFractionOfEnemiesInKernel",
            testAgent?.rootGameState?.players?.get("1"),
            10
        )

        assertEquals(score1, score2, "Expect function to be idempotent")

        assert(score1 in 0.0..1.0) { "Expect returned value to be between 0 and 1" }

        assertNotEquals(score1WithNewEnemy, score1, "Expect returned value to change when number of enemies changes")
    }

    @Test
    fun getFractionOfAvailableCellsInKernel() {
        val score1 = Whitebox.invokeMethod<Double>(
            testAgent,
            "getFractionOfAvailableCellsInKernel",
            testAgent?.rootGameState?.players?.get("1"),
            listOf<Cell>(),
            10
        )

        val score2 = Whitebox.invokeMethod<Double>(
            testAgent,
            "getFractionOfAvailableCellsInKernel",
            testAgent?.rootGameState?.players?.get("1"),
            listOf<Cell>(),
            10
        )

        val player = testAgent?.rootGameState?.players?.get("1")!!
        testAgent?.rootGameState?.cells?.get(player.y + 1)?.get(player.x + 1)?.value = 7

        val score1WithNewCell = Whitebox.invokeMethod<Double>(
            testAgent,
            "getFractionOfAvailableCellsInKernel",
            testAgent?.rootGameState?.players?.get("1"),
            listOf<Cell>(),
            10
        )

        // Expect function to be idempotent
        assertEquals(score1, score2, "Expect function to be idempotent")

        // Expect returned value to be between 0 and 1
        assert(score1 in 0.0..1.0) { "Expect returned value to be between 0 and 1" }

        // Expect returned value to change when number of enemies changes
        assertNotEquals(score1WithNewCell, score1, "Expect returned value to change when number of cells changes")
    }
}
