package agent.alternativ

import TestDataProvider
import game.Move
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Instant
import kotlin.test.assertEquals

internal class HeuristicAgentTest {

    private var testHeuristicAgent: HeuristicAgent? = null

    @BeforeEach
    fun setUp() {
        // Reset TestAgent
        testHeuristicAgent = HeuristicAgent(TestDataProvider().getTestGameState())
    }

    @Test
    fun makeMove() {
        val action = testHeuristicAgent!!.makeMove(Instant.now())

        assert(Move.values().any { move -> move.toAction() == action }) { "Expect function to return a valid move" }
    }

    @Test
    fun staticEval() {
        val player = testHeuristicAgent!!.rootGameState.players["1"]!!

        val score1 = testHeuristicAgent!!.staticEval(player, listOf(), 0)

        val score2 = testHeuristicAgent!!.staticEval(player, listOf(), 0)

        Assertions.assertEquals(score1, score2, "Expect function to be idempotent")

        assert(score1 in -2.0..1.0) { "Expect returned value to be between -2 and 1 but was $score1" }
    }

    @Test
    fun staticEvalWorstCase() {
        val agent = HeuristicAgent(TestDataProvider().getWorstCaseTestGameState())
        val player = agent.rootGameState.players["1"]!!

        val score1 = agent.staticEval(player, listOf(), 0)

        val score2 = agent.staticEval(player, listOf(), 0)

        // Expect function to be idempotent
        assertEquals(score1, score2, "Expect function to be idempotent")

        // Expect returned value to be between -2 and 1
        assert(score1 in -2.0..1.0) { "Expect returned value to be between -2 and 1 but was $score1" }
    }
}
