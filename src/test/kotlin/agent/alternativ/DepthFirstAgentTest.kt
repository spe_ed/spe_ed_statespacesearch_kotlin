package agent.alternativ

import TestDataProvider
import game.Move
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Instant
import kotlin.test.assertEquals

internal class DepthFirstAgentTest {

    private var testDepthFirstAgent: DepthFirstAgent? = null

    @BeforeEach
    fun setUp() {
        // Reset TestAgent
        testDepthFirstAgent = DepthFirstAgent(TestDataProvider().getTestGameState())
    }

    @Test
    fun makeMove() {
        val action = testDepthFirstAgent!!.makeMove(Instant.now())

        assert(Move.values().any { move -> move.toAction() == action }) { "Expect function to return a valid move" }
    }

    @Test
    fun makeMoveNoTime() {
        val action2 = testDepthFirstAgent!!.makeMove(Instant.now().plusSeconds(11))

        assertEquals(
            Move.change_nothing.toAction(),
            action2,
            "Expect function to return default move when there is no calculation time"
        )
    }

    @Test
    fun staticEval() {
        val player = testDepthFirstAgent!!.rootGameState.players["1"]!!

        val score1 = testDepthFirstAgent!!.staticEval(player, listOf(), 0)

        val score2 = testDepthFirstAgent!!.staticEval(player, listOf(), 0)

        Assertions.assertEquals(score1, score2, "Expect function to be idempotent")

        assert(score1 in 0.0..1.0) { "Expect returned value to be between 0 and 1 but was $score1" }
    }

    @Test
    fun staticEvalWorstCase() {
        val agent = DepthFirstAgent(TestDataProvider().getWorstCaseTestGameState())
        val player = agent.rootGameState.players["1"]!!

        val score1 = agent.staticEval(player, listOf(), 0)

        val score2 = agent.staticEval(player, listOf(), 0)

        assertEquals(score1, score2, "Expect function to be idempotent")

        assert(score1 in 0.0..1.0) { "Expect returned value to be between 0 and 1 but was $score1" }
    }
}
