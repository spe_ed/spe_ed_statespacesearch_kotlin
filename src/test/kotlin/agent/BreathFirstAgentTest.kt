package agent

import TestDataProvider
import game.Move
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Instant
import kotlin.test.assertEquals

internal class BreathFirstAgentTest {

    private var testBFSAgent: BreathFirstAgent? = null

    @BeforeEach
    fun setUp() {
        // Reset TestAgent
        testBFSAgent = BreathFirstAgent(TestDataProvider().getTestGameState())
    }

    @Test
    fun makeMove() {
        val action = testBFSAgent!!.makeMove(Instant.now())

        assert(Move.values().any { move -> move.toAction() == action }) { "Expect function to return a valid move" }
    }

    @Test
    fun makeMoveNoTime() {
        val action2 = testBFSAgent!!.makeMove(Instant.now().plusSeconds(11))

        assertEquals(
            Move.change_nothing.toAction(),
            action2,
            "Expect function to return default move when there is no calculation time"
        )
    }

    @Test
    fun staticEval() {
        val player = testBFSAgent!!.rootGameState.players["1"]!!

        val score1 = testBFSAgent!!.staticEval(player, listOf(), 0)

        val score2 = testBFSAgent!!.staticEval(player, listOf(), 0)

        assertEquals(score1, score2, "Expect function to be idempotent")

        assert(score1 >= 0) { "Expect returned value to be positive but was $score1" }
    }

    @Test
    fun staticEvalWorstCase() {
        val agent = BreathFirstAgent(TestDataProvider().getWorstCaseTestGameState())
        val player = agent.rootGameState.players["1"]!!

        val score1 = agent.staticEval(player, listOf(), 0)

        val score2 = agent.staticEval(player, listOf(), 0)

        assertEquals(score1, score2, "Expect function to be idempotent")

        assertEquals(score1, 0.0, "Expect returned value to be 0 but was $score1")
    }
}
