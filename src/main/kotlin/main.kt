import agent.BreathFirstAgent
import agent.Viewer
import com.beust.klaxon.Klaxon
import com.importre.crayon.underline
import game.GameState
import game.ParsedGameState
import game.Time
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.websocket.*
import io.ktor.client.request.*
import io.ktor.http.cio.websocket.*
import io.ktor.util.*
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.runBlocking

/**
 * Starts the agents connection to the websocket at the location specified by the environment variables
 */
// Websockets are still experimental
@KtorExperimentalAPI
fun main() {
    val envURL: String = System.getenv("URL") ?: "localhost"
    val envKEY: String = System.getenv("KEY")
    val envTIME: String = System.getenv("TIME_URL")
    val envDISPLAY: Boolean = System.getenv("DISPLAY_GAME").toBoolean()

    val client = HttpClient(CIO) {
        install(WebSockets)
    }

    runBlocking {
        handleWebsockets(client, envURL, envTIME, envKEY, envDISPLAY)
    }
}

/**
 * Handle websockets
 *
 * @param client over which to start the connection
 * @param url to send the wss request to
 * @param time_url to get the server time
 * @param key the api key
 * @param displayImage whether to display the game state as image
 */
suspend fun handleWebsockets(client: HttpClient, url: String, time_url: String, key: String, displayImage: Boolean) {
    val viewer = if (displayImage) Viewer() else null
    // Connect to the specified websocket
    println("Connecting to $url")
    client.wss(
        urlString = "$url?key=$key"
    ) {
        var round = 1
        while (true) {
            // Receive websocket frame.
            // Should contain the game state information of the current round
            val frame: Frame
            try {
                frame = incoming.receive()
            } catch (exception: ClosedReceiveChannelException) {
                println(exception.message)
                break
            }

            if (frame is Frame.Text) {
                // Parse the received game state from json to the GameState class using Klaxon
                when (val gameState = Klaxon().parse<GameState>(frame.readText())) {
                    null -> {
                        // If parsing failed, stop the program
                        println("Parsing failed")
                        return@wss
                    }
                    else -> {
                        // Display the new game state if viewer has been set
                        viewer?.update(gameState.cells, gameState.you)
                        if (!gameState.running) {
                            println("Game ended")
                            return@wss
                        }

                        // Calculate move if own player is still alive
                        if (gameState.players[gameState.you.toString()]?.active == true && gameState.running) {
                            println("Round $round: ${gameState.players.filter { (_, player) -> player.active }.size} players alive".underline())
                            // Get and parse server time to calculate offset
                            val serverTime =
                                Klaxon().parse<Time>(client.get<String>(time_url))

                            if (serverTime !== null) {
                                // Create Agent and calculate move to make, then send it
                                send(
                                    BreathFirstAgent(
                                        ParsedGameState(
                                            round,
                                            gameState
                                        )
                                    ).makeMove(serverTime.toInstant())
                                )
                                round++
                            }
                        }
                    }
                }
            }
        }
    }
}
