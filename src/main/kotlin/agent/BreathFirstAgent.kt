package agent

import com.importre.crayon.bold
import com.importre.crayon.italic
import game.Cell
import game.Move
import game.ParsedGameState
import game.Player
import java.time.Duration
import java.time.Instant
import java.util.*
import kotlin.math.pow

/**
 * Breath first agent
 *
 * Implements a player for the game spe_ed which uses breath first state space search to determine the best move.
 *
 * @constructor
 *
 * @param gameState the current state of the game, used as root in state space search tree
 */
class BreathFirstAgent(gameState: ParsedGameState) : Agent(gameState) {

    private val statesToFurtherInvestigate: Queue<State> = LinkedList()

    private val numberOfStatesPerMove = HashMap<Move, Int>()

    private val scorePerMove = HashMap<Move, Double>()

    private val fractionOfTimeToBeUsedForCalculation = 0.9

    /**
     * Calculate which of the 5 moves is the best to be used in the current root game state
     *
     * @param roundStartTime time at the start of the round
     * @return json representation of the calculated move
     */
    override fun makeMove(roundStartTime: Instant): String {
        // Create first 5 subtrees, setting the root move correspondingly
        Move.values().map { move ->
            val state = State(listOf(move), move, evalState(listOf(move)))
            if (state.score != null) {
                statesToFurtherInvestigate.add(state)
                numberOfStatesPerMove[move] = 1
                scorePerMove[move] = state.score
            }
        }

        // Calculate the time the player can use to determine the move
        val startTime = System.currentTimeMillis()
        val calculationTime =
            Duration.between(roundStartTime, rootGameState.deadline).toMillis() * fractionOfTimeToBeUsedForCalculation

        // Investigate a new state as long there is time left and there are still states to further investigate
        while (System.currentTimeMillis() - startTime < calculationTime) {
            if (statesToFurtherInvestigate.isNotEmpty()) {
                // Get next state from queue
                val state = statesToFurtherInvestigate.remove()
                // Get number of states in subtree
                val amountStates = numberOfStatesPerMove[state.rootMove]
                // Get score of subtree
                val score = scorePerMove[state.rootMove]
                if (amountStates !== null && score !== null) {
                    // Create 5 new nodes
                    Move.values().map { move ->
                        val newState = State(
                            state.moves + listOf(move),
                            state.rootMove,
                            evalState(state.moves + listOf(move))
                        )
                        // If state is valid, update the score and amount and queue it to be further investigated
                        if (newState.score !== null) {
                            numberOfStatesPerMove[state.rootMove] = amountStates + 1
                            scorePerMove[state.rootMove] = score + newState.score
                            statesToFurtherInvestigate.add(
                                newState
                            )
                        } else {
                            numberOfStatesPerMove[state.rootMove] = amountStates + 1
                        }
                    }
                }
            } else {
                break
            }
        }
        // Get move with best score
        val bestMoveEval = scorePerMove.maxByOrNull { (_, score) -> score }
        if (bestMoveEval === null) {
            return Move.change_nothing.toAction().also { println("Oh no! Picking ${Move.change_nothing.name}\n".bold()) }
        }

        // Clear Queue, not strictly necessary
        statesToFurtherInvestigate.clear()

        val (move, _) = bestMoveEval
        println("Scores:".italic())
        scorePerMove.forEach { (move, score) -> println(move.paint("${move.name}: %.2f".format(score))) }
        println()
        println("Number of States:".italic())
        numberOfStatesPerMove.forEach { (move, amount) -> println(move.paint("${move.name}: $amount")) }
        println()
        return move.toAction()
            .also { println("Picking ".bold() + move.paint(move.name) + " with score ${scorePerMove[move]} and ${numberOfStatesPerMove[move]} states\n".bold()) }
    }

    /**
     * Override the static eval function of [Agent]
     * For further documentation view [Agent.staticEval]
     */
    override fun staticEval(player: Player, blockedCells: List<Cell>, stepsInTheFuture: Int): Double {
        // The number of available cells in the vicinity of the player and the number of enemies as well as the player speed are used to evaluate the state
        // The Fractions lie between 0 and 1 and the speed between 0 and 10
        // The further in the future the current state is
        // the worse our enemy positions align with the actually possible positions,
        // as we dont update them. Therefore we

        return getFractionOfAvailableCellsInKernel(player, blockedCells) * (
            1 - 0.99.pow(stepsInTheFuture) * getFractionOfEnemiesInKernel(
                player,

                5
            )
            ) * (10 - player.speed.toDouble()).pow(2)
    }
}
