package agent

import game.*
import game.ParsedGameState
import java.time.Instant
import kotlin.math.max
import kotlin.math.min
import kotlin.math.pow

/**
 * Agent
 *
 * @constructor
 *
 * @param gameState
 */
abstract class Agent(gameState: ParsedGameState) {

    val rootGameState: ParsedGameState = gameState

    /**
     * Calculate which of the 5 moves is the best to be used in the current root game state
     *
     * @param roundStartTime time at the start of the round
     * @return json representation of the calculated move
     */
    abstract fun makeMove(roundStartTime: Instant): String

    /**
     * Eval a single state
     *
     * @param moves that the player has to take to get from the root game state to the intended game state
     * @return a score for the state
     */
    protected fun evalState(moves: List<Move>): Double? {
        // Use moves to transform root game state into current state
        val gameState = getGameStateFromMoves(moves)

        // Mark state as invalid if transformation failed
        if (gameState === null) {
            return null
        }

        val (blockedCells, player) = gameState

        // Get score for state
        return staticEval(player, blockedCells, moves.size)
    }

    /**
     * Get game state from moves
     *
     * Iterate over the moves and transform the game state one by one
     *
     * @param moves that the player has to take to get from the root game state to the intended game state
     * @return the newly blocked cells and the new player info representing the game state
     */
    private fun getGameStateFromMoves(moves: List<Move>): Pair<List<Cell>, Player>? {
        var blockedCells: List<Cell> = listOf()
        var newPlayer: Player =
            this.rootGameState.players[this.rootGameState.you.toString()] ?: throw Exception("Player not found")
        var round = this.rootGameState.round

        moves.forEach { move ->
            val newState = getGameStateFromMove(move, blockedCells, newPlayer, round)
            round++
            // If transformation failed return null
            if (newState === null) {
                return null
            }
            val (cells, player) = newState
            blockedCells = cells
            newPlayer = player
        }

        return Pair(blockedCells, newPlayer)
    }

    /**
     * Get game state from move
     *
     * @param move the move to apply to the game state represented by [blockedCells] and [oldPlayer]
     * @param blockedCells the cells that are blocked in addition to the cells which are blocked in the root game state
     * @param oldPlayer the player info
     * @param round the current round
     * @return a new game state in which the move has been made or null if the state is invalid
     */
    private fun getGameStateFromMove(
        move: Move,
        blockedCells: List<Cell>,
        oldPlayer: Player,
        round: Int
    ): Pair<List<Cell>, Player>? {
        // Copy to not change the values of the parent object
        val player = oldPlayer.copy()
        // Adjust speed
        player.speed = when (move) {
            Move.speed_up -> {
                player.speed + 1
            }
            Move.slow_down -> {
                player.speed - 1
            }
            else -> {
                player.speed
            }
        }

        // return null if speed is invalid
        if (player.speed <= 0 || player.speed > 10) {
            return null
        }

        // Adjust direction
        player.direction = when (move) {
            Move.turn_left -> {
                player.direction.leftTurn()
            }
            Move.turn_right -> {
                player.direction.rightTurn()
            }
            else -> {
                player.direction
            }
        }

        return movePlayer(player, blockedCells, round)
    }

    /**
     * Move player over the game board
     *
     * @param player the player info to move
     * @param blockedCells the cells which are already blocked in addition to the cells which are blocked in the root game state
     * @param round the current round
     * @return a new game state in which the player has been moved
     */
    private fun movePlayer(player: Player, blockedCells: List<Cell>, round: Int): Pair<List<Cell>, Player>? {
        var isJump = false
        val cells = mutableListOf<Cell>()
        cells.addAll(blockedCells)
        // Move the player for player.speed number of steps
        for (i in 0 until player.speed) {
            // If the round is one where the player can jump and one step has been taken, set the
            val stepSize: Int = if (round % 6 == 0 && player.speed >= 3 && isJump) {
                player.speed - 1
            } else {
                1
            }
            // Change position
            when (player.direction) {
                Direction.up -> player.y -= stepSize
                Direction.right -> player.x += stepSize
                Direction.down -> player.y += stepSize
                Direction.left -> player.x -= stepSize
            }
            // Check if move was valid
            if (isOutsideField(
                player,
                this.rootGameState
            ) || this.rootGameState.cells[player.y][player.x].value != 0 || blockedCells.contains(
                Cell(
                    player.x,
                    player.y,
                    1
                )
            )
            ) {
                return null
            }
            // Add cells to newly blocked cells
            cells.add(Cell(player.x, player.y, 1))
            // Break if this is a jump round and we already changed the player position by player.speed - 1 to emulate the jump
            if (round % 6 == 0 && player.speed >= 3) {
                if (isJump) {
                    break
                } else {
                    isJump = true
                }
            }
        }
        // Return new game state
        return Pair(cells, player)
    }

    /**
     * Evaluate a state without calculating extra steps
     *
     * @param player the player info
     * @param blockedCells the cells which are already blocked in addition to the cells which are blocked in the root game state
     * @param stepsInTheFuture number of steps the agent has calculated into the future to get to this state
     * @return a score for the state
     */
    abstract fun staticEval(player: Player, blockedCells: List<Cell>, stepsInTheFuture: Int): Double

    /**
     * Get fraction of enemies in Vicinity
     *
     * @param player the player info
     * @param radius size of search window, side length of square is 2 * radius + 1
     * @return the amount of enemies in the search window divided by the amount of players - 1
     */
    protected fun getFractionOfEnemiesInKernel(player: Player, radius: Int = 10): Double {
        // If there are no enemies return 0.0
        return if (rootGameState.players.size > 1) {
            // Get positions of enemies in root game state
            val playerPositions = rootGameState.players
                .filter { (index, _) -> index != rootGameState.you.toString() }
                .map { (_, player) -> Pair(player.x, player.y) }
            // Return fraction
            getAllCellsInKernel(player, radius)
                .flatten()
                .filter { cell -> cell !== null && playerPositions.contains(Pair(cell.x, cell.y)) }
                .size.toDouble() / (rootGameState.players.size - 1).toDouble()
        } else {
            0.0
        }
    }

    /**
     * Get fraction of available cells in kernel using connected component analysis
     *
     * @param player the player info
     * @param blockedCells the cells which are already blocked in addition to the cells which are blocked in the root game state
     * @param radius size of search window, side length of square is 2 * radius + 1
     * @return fraction of available cells     */
    protected fun getFractionOfAvailableCellsInKernel(
        player: Player,
        blockedCells: List<Cell>,
        radius: Int = 10
    ): Double {
        // Only cells from root game state, others will be checked later
        val cells = getAllCellsInKernel(player, radius)

        val groups: MutableList<MutableList<Int?>> = mutableListOf()
        val mappings: MutableMap<Int, Int> = mutableMapOf()

        // First Pass
        var groupId = 0

        cells.mapIndexed { y, row ->
            groups.add(y, mutableListOf())
            row.mapIndexed { x, cell ->
                // For each cell, row by row, assign a group based on left and down cell as well as cell value

                // If cell is wall or not the player head or blocked by moves we will have made: set group to null
                if (cell != cells[radius][radius] &&
                    (
                        cell === null || cell.value != 0 || blockedCells.contains(
                            Cell(
                                cell.x,
                                cell.y,
                                1
                            )
                        )
                        )
                ) {
                    groups[y].add(x, null)
                } else {
                    // Cell is empty or player head
                    val ownGroupId: Int

                    // Try to get group of up cell
                    val up: Int? = try {
                        groups[y - 1][x]
                    } catch (exception: IndexOutOfBoundsException) {
                        null
                    }
                    // Try to get group of left cell
                    val left: Int? = try {
                        groups[y][x - 1]
                    } catch (exception: IndexOutOfBoundsException) {
                        null
                    }

                    if (up !== null && left !== null) {
                        // Both cells have a group, take min
                        ownGroupId = min(up, left)
                        // if groups are not the same, because they are now touching,
                        // create mapping so they will be the same group after the second step
                        if (left != up) {
                            mappings.putIfAbsent(max(left, up), min(left, up))
                        }
                    } else if (up !== null) {
                        ownGroupId = up
                    } else if (left !== null) {
                        ownGroupId = left
                    } else {
                        // No cells have a group, create a new one
                        groupId++
                        ownGroupId = groupId
                    }
                    groups[y].add(x, ownGroupId)
                }
            }
        }

        // Second Pass
        // Use Mappings to combine touching groups
        groups.mapIndexed { y, row ->
            row.mapIndexed { x, group ->
                if (group != null) {
                    var newGroup = group
                    while (mappings.containsKey(newGroup)) {
                        newGroup = mappings[newGroup]
                    }
                    groups[y][x] = newGroup
                }
            }
        }

        // Count cells in players group
        var amountOfEmptyAccessibleCells = 0.0
        val groupOfPlayer = groups[radius][radius]
        groups.map { row ->
            row.map { group ->
                if (group == groupOfPlayer) {
                    amountOfEmptyAccessibleCells++
                }
            }
        }

        // Calculate fraction amountOfEmptyAccessibleCells / kernel size
        return (amountOfEmptyAccessibleCells - 1) / (
            groups.size.toDouble()
                .pow(2)
            ) // minus 1 because player head is already taking up a space
    }

    /**
     * Get all cells in kernel
     *
     * @param player the player info
     * @param radius size of search window, side length of square is 2 * radius + 1
     * @return the cells from the game board in the radius
     */
    private fun getAllCellsInKernel(player: Player, radius: Int): List<List<Cell?>> {
        val cells: MutableList<MutableList<Cell?>> = mutableListOf()
        (-radius..radius).mapIndexed { y, yOffset ->
            cells.add(y, mutableListOf())
            (-radius..radius).mapIndexed { x, xOffset ->
                if (isOutsideField(
                    player.x + xOffset,
                    player.y + yOffset,
                    this.rootGameState.width,
                    this.rootGameState.height
                )
                ) {
                    cells[y].add(x, null)
                } else {
                    cells[y].add(x, this.rootGameState.cells[player.y + yOffset][player.x + xOffset])
                }
            }
        }
        return cells
    }

    companion object {
        /**
         * Is outside field
         *
         * @param player the player info
         * @param gameState a root game state to take width and height from
         * @return whether the player is outside the field
         */
        private fun isOutsideField(player: Player, gameState: ParsedGameState): Boolean {
            return (player.x < 0 || player.x >= gameState.width || player.y >= gameState.height || player.y < 0)
        }

        /**
         * Is outside field
         *
         * @param x player x position
         * @param y player y position
         * @param width game board width
         * @param height game board height
         * @return whether the player is outside the field
         */
        private fun isOutsideField(x: Int, y: Int, width: Int, height: Int): Boolean {
            return (x < 0 || x >= width || y >= height || y < 0)
        }
    }
}
