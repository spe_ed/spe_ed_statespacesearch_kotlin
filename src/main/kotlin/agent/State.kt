package agent

import game.Move

/**
 * State representing a game state reached by acting out the [moves] in order on the root game state
 *
 * @property moves list of moves to take from the root game state to reach this state
 * @property rootMove corresponding to the first move in the path through the state space to reach this state
 * @property score of this state
 * @constructor Create State
 */
data class State(
    val moves: List<Move>,
    val rootMove: Move,
    val score: Double?
) : Comparable<State> {
    /**
     * Compares this object with the specified object for order. Returns zero if this object is equal
     * to the specified [other] object, a negative number if it's greater than [other], or a positive number
     * if it's less than [other].
     *
     * This is against the convention but useful for our implementation of the HeuristicAgent and its priority queue
     */
    override fun compareTo(other: State): Int {
        return when {
            score === null || other.score === null -> throw Exception("Cant compare states if either is null")
            score > other.score -> -1
            score < other.score -> 1
            else -> 0
        }
    }
}
