package agent

import java.awt.Color
import java.awt.FlowLayout
import java.awt.image.BufferedImage
import javax.swing.ImageIcon
import javax.swing.JFrame
import javax.swing.JLabel

/**
 * Viewer to show the current game state in a JFrame
 *
 * Own player color is red
 *
 * @constructor Create Viewer
 */
class Viewer {

    private val frame = JFrame()
    private val label = JLabel()
    private val colors = listOf<Color>(Color.BLACK, Color.BLUE, Color.DARK_GRAY, Color.GREEN, Color.MAGENTA, Color.PINK, Color.WHITE, Color.CYAN)

    init {
        frame.layout = FlowLayout()
        frame.isAlwaysOnTop = true
        frame.isResizable = false
        frame.add(label)
        frame.isVisible = true
        frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
    }

    /**
     * Get image from [pixels]
     *
     * @param pixels 2D list of values from the game board(0 -> empty, -1 -> collision, else -> player)
     * @param you is the player number of your player
     * @return [BufferedImage] of the game board
     */
    private fun getImageFromArray(pixels: List<List<Int>>, you: Int): BufferedImage {
        val image = BufferedImage(pixels[0].size, pixels.size, BufferedImage.TYPE_INT_RGB)
        pixels.mapIndexed { y, row ->
            row.mapIndexed { x, value ->
                image.setRGB(x, y, if (value == -1) Color.white.rgb else if (value == you) Color.RED.rgb else colors[value].rgb)
            }
        }
        return image
    }

    /**
     * Update the frame
     *
     * @param pixels 2D list of values from the game board(0 -> empty, -1 -> collision, else -> player)
     * @param you is the player number of your player
     */
    fun update(pixels: List<List<Int>>, you: Int) {
        frame.setSize(pixels[0].size * 10 + 20, pixels.size * 10 + 50)
        val icon = ImageIcon(getImageFromArray(pixels, you).getScaledInstance(pixels[0].size * 10, pixels.size * 10, 1))
        label.icon = icon
        frame.repaint()
    }
}
