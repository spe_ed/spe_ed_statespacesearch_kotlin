package agent.alternativ

import agent.Agent
import agent.State
import game.Cell
import game.Move
import game.ParsedGameState
import game.Player
import java.time.Duration
import java.time.Instant
import java.util.Deque
import java.util.LinkedList

/**
 * Depth first agent
 *
 * Implements a player for the game spe_ed which uses depth first state space search to determine the best move.
 *
 * @constructor
 *
 * @param gameState the current state of the game, used as root in state space search tree
 */
class DepthFirstAgent(gameState: ParsedGameState) : Agent(gameState) {

    private val statesToBeEvaluated: Deque<State> = LinkedList()

    private var bestState: State = State(listOf(), Move.change_nothing, 0.0)

    private val fractionOfTimeToBeUsedForCalculation = 0.03

    private val depth = 9

    /**
     * Calculate which of the 5 moves is the best to be used in the current root game state
     *
     * @param roundStartTime time at the start of the round
     * @return json representation of the calculated move
     */
    override fun makeMove(roundStartTime: Instant): String {
        // Create first 5 subtrees, setting the root move correspondingly
        Move.values().map { move ->
            statesToBeEvaluated.add(State(listOf(move), move, 0.0))
        }

        // Calculate the time the player can use to determine the move
        val startTime = System.currentTimeMillis()
        val calculationTime =
            Duration.between(roundStartTime, rootGameState.deadline).toMillis() * fractionOfTimeToBeUsedForCalculation

        // Investigate a new state as long there is time left and there are still states to further investigate
        while (System.currentTimeMillis() - startTime < calculationTime) {
            if (statesToBeEvaluated.isNotEmpty()) {
                // Get next state from dequeue
                val state = statesToBeEvaluated.remove()
                if (state.moves.size <= depth) {
                    val score = evalState(state.moves)
                    if (score !== null) {
                        Move.values().map { move ->
                            statesToBeEvaluated.addFirst(
                                State(
                                    state.moves + listOf(move),
                                    state.rootMove,
                                    score
                                )
                            )
                        }
                    }
                } else {
                    if (state.score !== null && state.score >= bestState.score!!) {
                        bestState = state
                    }
                }
            } else {
                break
            }
        }
        for (state in statesToBeEvaluated) {
            if (state.score !== null && state.score >= bestState.score!!) {
                bestState = state
            }
        }

        return bestState.rootMove.toAction()
            .also { println("Picking ${bestState.rootMove} with score ${bestState.score} at depth ${bestState.moves.size}") }
    }

    /**
     * Override the static eval function of [Agent]
     * For further documentation view [Agent.staticEval]
     */
    override fun staticEval(player: Player, blockedCells: List<Cell>, stepsInTheFuture: Int): Double {
        return getFractionOfAvailableCellsInKernel(player, blockedCells)
    }
}
