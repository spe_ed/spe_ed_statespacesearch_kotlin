package agent.alternativ

import agent.Agent
import agent.State
import game.Cell
import game.Move
import game.ParsedGameState
import game.Player
import java.time.Duration
import java.time.Instant
import java.util.*

/**
 * Heuristic agent
 *
 * Implements a player for the game spe_ed which uses heuristic state space search to determine the best move.
 *
 * @constructor
 *
 * @param gameState the current state of the game, used as root in state space search tree
 */
class HeuristicAgent(gameState: ParsedGameState) : Agent(gameState) {

    private val statesToBeEvaluated: Queue<State> = PriorityQueue()

    private val meanScoresPerMove = HashMap<Move, Double>()

    private val numberOfStatesPerMove = HashMap<Move, Int>()

    private val fractionOfTimeToBeUsedForCalculation = 0.03

    /**
     * Calculate which of the 5 moves is the best to be used in the current root game state
     *
     * @param roundStartTime time at the start of the round
     * @return json representation of the calculated move
     */
    override fun makeMove(roundStartTime: Instant): String {
        // Create first 5 subtrees, setting the root move correspondingly
        Move.values().map { move ->
            val state = State(listOf(move), move, evalState(listOf(move)))
            if (state.score != null) {
                statesToBeEvaluated.add(state)
                numberOfStatesPerMove[move] = 1
                meanScoresPerMove[move] = state.score
            }
        }

        // Calculate the time the player can use to determine the move
        val startTime = System.currentTimeMillis()
        val calculationTime =
            Duration.between(roundStartTime, rootGameState.deadline).toMillis() * fractionOfTimeToBeUsedForCalculation

        // Investigate a new state as long there is time left and there are still states to further investigate
        while (System.currentTimeMillis() - startTime < calculationTime) {
            if (statesToBeEvaluated.isNotEmpty()) {
                // Get next state from priority queue
                val state = statesToBeEvaluated.remove()
                // Get mean score of subtree
                val meanScore = meanScoresPerMove[state.rootMove]
                // Get number of states in subtree
                val amountStates = numberOfStatesPerMove[state.rootMove]
                if (meanScore !== null && amountStates !== null) {
                    // Create 5 new nodes
                    Move.values().map { move ->
                        val newState = State(
                            state.moves + listOf(move),
                            state.rootMove,
                            evalState(state.moves + listOf(move))
                        )
                        // If state is valid, update the score and amount and queue it to be further investigated
                        if (newState.score !== null) {
                            meanScoresPerMove[state.rootMove] = (meanScore + newState.score) / 2
                            numberOfStatesPerMove[state.rootMove] = amountStates + 1
                            statesToBeEvaluated.add(
                                newState
                            )
                        } else {
                            meanScoresPerMove[state.rootMove] = (meanScore + -2) / 2
                            numberOfStatesPerMove[state.rootMove] = amountStates + 1
                        }
                    }
                }
            }
        }

        // Get move with highest amount
        val bestMoveEval = numberOfStatesPerMove.maxByOrNull { (_, score) -> score }

        if (bestMoveEval === null) {
            println()
            println(meanScoresPerMove)
            println(numberOfStatesPerMove)
            return Move.change_nothing.toAction().also { println("Oh no! Picking ${Move.change_nothing.name}") }
        }

        val (move, _) = bestMoveEval
        println()
        println(meanScoresPerMove)
        println(numberOfStatesPerMove)
        return move.toAction().also { println("Picking ${move.name} with score ${meanScoresPerMove[move]} and ${numberOfStatesPerMove[move]} states") }
    }

    /**
     * Override the static eval function of [Agent]
     * For further documentation view [Agent.staticEval]
     */
    override fun staticEval(player: Player, blockedCells: List<Cell>, stepsInTheFuture: Int): Double {
        // The number of available cells in the vicinity of the player and the number of enemies are used to evaluate the state
        // The Fractions lie between 0 and 1, getting remapped to -1 and 1 so prioritization of states is easier
        return 2 * (
            (
                getFractionOfAvailableCellsInKernel(player, blockedCells) + (
                    getFractionOfEnemiesInKernel(
                        player,
                        stepsInTheFuture
                    ) - 1
                    ) / 2
                )
            ) - 1
    }
}
