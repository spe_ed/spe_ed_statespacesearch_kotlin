package game

/**
 * Game state
 *
 * @property width of the game board
 * @property height of the game board
 * @property cells is a 2D list with the cell values of the game board(0 -> empty, -1 -> collision, else -> player)
 * @property players is a list which contains information about the players
 * @property you is your player number
 * @property running indicates whether the game is running
 * @property deadline is the date by which you must have send a move
 * @constructor Create a GameState
 */
data class GameState(
    val width: Int,
    val height: Int,
    val cells: List<List<Int>>,
    val players: Map<String, Player>,
    val you: Int,
    val running: Boolean,
    val deadline: String = ""
)
