package game

import java.time.Instant

/**
 * Parsed game state
 *
 * @property round of the game
 * @constructor
 *
 * @param gameState from the current round of the game
 */
class ParsedGameState(val round: Int, gameState: GameState) {
    val width: Int = gameState.width
    val height: Int = gameState.height
    val players: Map<String, Player> = gameState.players
    val you: Int = gameState.you
    var cells: List<List<Cell>> =
        gameState.cells.mapIndexed { y, row -> row.mapIndexed { x, value -> Cell(x, y, if (value != 0) 1 else 0) } }
    val deadline: Instant = Instant.parse(gameState.deadline)
}
