rootProject.name = "StateSpaceSearch_Kotlin"
enableFeaturePreview("GRADLE_METADATA")

pluginManagement {
    repositories {
        gradlePluginPortal()
        jcenter()
    }
}
